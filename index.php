<?php
$startTIME = microtime(true);
error_reporting(E_ALL);
ini_set('display_errors', 'On');

session_name('TSN_RANKSYSTEM');
session_start();

require_once 'system/bootstrap.php';

$router = new TSN_Router();
$router->handle();
