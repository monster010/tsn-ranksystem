<?php
class TSN_Input {
	private $post;
	private $get;

	public function __construct() {
		$this->post = $_POST;
		$this->get = $_GET;
	}

	public function post($key) {
		if(isset($this->post[$key]))
			return $this->post[$key];

		return null;
	}

	public function get($key) {
		if(isset($this->get[$key]))
			return $this->get[$key];

		return null;
	}

	public function server($key) {
		if(isset($_SERVER[$key]))
			return $_SERVER[$key];

		return null;
	}

	public function cookie($key) {
		if(isset($_COOKIE[$key]))
			return $_COOKIE[$key];

		return null;
	}
}
