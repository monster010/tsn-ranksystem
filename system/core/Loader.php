<?php
class TSN_Loader {
	public function __construct() {
		spl_autoload_register(array($this, 'loadClass'));
	}

	public function model($model, $params = null, $object_name = null) {
		$tsn = getInstance();
		$modelName = $model.'_model';
		$file = APP_PATH.'models/'.$model.'.php';

		if(!file_exists($file)) return;

		require $file;

		if(!is_null($params)) {
			if(!is_null($object_name)) $tsn->{$object_name} = new $modelName($params);
			else $tsn->{$model} = new $modelName($params);
		} else {
			if(!is_null($object_name)) $tsn->{$object_name} = new $modelName();
			else $tsn->{$model} = new $modelName();
		}
	}

	public function database() {
		$tsn =& getInstance();
		return getInstance()->db;
	}

	public function library($library, $params = null) {

	}

	public function helper($helper) {
		if(!is_array($helper)) $helper = array($helper);

		foreach($helper as $help) {
			$file = SYSTEM_PATH.'helper/'.$help.'_helper.php';

			if(file_exists($file))
				require_once $file;
		}
	}

	public function modal($modal, array $vars = array()) {
		$file = APP_PATH.'modals/'.$modal.'.php';

		if(!file_exists($file)) return;

		extract($vars);

		ob_start();
		include $file;
		echo TSN_Minify::html(ob_get_clean());
	}

	public function view($view, array $vars = array()) {
		$file = APP_PATH.'views/'.$view.'.php';

		if(!file_exists($file)) show_404();
		
		extract($vars);

		ob_start();
		include $file;
		echo TSN_Minify::html(ob_get_clean());
	}

	private function loadClass($class) {
		$class = str_replace('TSN_', '', $class);
		$file = SYSTEM_PATH.'core/'.$class.'.php';

		if(file_exists($file))
			require_once $file;
	}
}
