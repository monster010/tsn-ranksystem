<?php
class TSN_Database {
	private $pdo;
	private $sQuery;
	private $connected = false;
	private $parameters;
	private $config;

	public function __construct() {
		include_once APP_PATH.'config.php';
		$this->config = $config;

		$this->connect();
		$this->parameters = array();
	}

	public function connect() {
		$dsn = 'mysql:dbname='.$this->config['db_name'].';host='.$this->config['db_host'].';port='.$this->config['db_port'].'';

		try {
			$this->pdo = new PDO($dsn, $this->config['db_user'], $this->config['db_pass'], array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
			));

			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

			$this->connected = true;
		} catch(PDOException $e) {
			die($e->getMessage());
		}
	}

	public function close() {
		$this->pdo = null;
	}

	public function bind($parar, $value = null) {
		if(is_array($parar)) {
			$columns = array_keys($parar);

			foreach($columns as $i => &$column) {
				$this->parameters[sizeof($this->parameters)] = [":".$column, $parar[$column]];
			}
		} else {
			$this->parameters[sizeof($this->parameters)] = [":".$parar, $value];
		}
	}

	public function query($query, $params = null, $mode = PDO::FETCH_ASSOC) {
		$query = trim(str_replace("\r", " ", $query));
		$this->init($query, $params);
		$rawStatement = explode(" ", preg_replace("/\s+|\t+|\n+/", " ", $query));
		$statement = strtolower($rawStatement[0]);

		if($statement === 'select' || $statement === 'show') {
			return $this->sQuery->fetchAll($mode);
		} elseif($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
			return $this->sQuery->rowCount();
		} else {
			return null;
		}
	}

	public function column($query, $params = null) {
		$this->init($query, $params);
		$columns = $this->sQuery->fetchAll(PDO::FETCH_NUM);
		$column = null;

		foreach($columns as $cells) {
			$column[] = $cells[0];
		}

		return $column;
	}

	public function row($query, $params = null, $mode = PDO::FETCH_ASSOC) {
		$this->init($query, $params);
		$result = $this->sQuery->fetch();
		$this->sQuery->closeCursor();

		return $result;
	}

	public function single($query, $params = null) {
		$this->init($query, $params);
		$result = $this->sQuery->fetchColumn();
		$this->sQuery->closeCursor();

		return $result;
	}

	public function lastInsertId() {
		return $this->pdo->lastInsertId();
	}

	public function beginTransaction() {
		return $this->pdo->beginTransaction();
	}

	public function commit() {
		return $this->pdo->commit();
	}

	public function rollback() {
		return $this->pdo->rollBack();
	}


	private function init($query, $params = null) {
		if(!$this->connected) $this->connect();

		try {
			$this->sQuery = $this->pdo->prepare($query);
			$this->bind($params);

			if(!empty($this->paramaters)) {
				foreach($this->parameters as $param => $value) {
					if(is_int($value[1])) $type = PDO::PARAM_INT;
					elseif(is_bool($value[1])) $type = PDO::PARAM_BOOL;
					elseif(is_null($value[1])) $type = PDO::PARAM_NULL;
					else $type = PDO::PARAM_STR;

					$this->sQuery->bindValue($value[0], $value[1], $type);
				}
			}

			$this->sQuery->execute();
		} catch(PDOException $e) {
			die($e->getMessage());
		}

		$this->parameters = array();
	}
}
