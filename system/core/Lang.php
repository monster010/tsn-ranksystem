<?php
class TSN_Lang {
	private $_languages_path = APP_PATH.'languages/';
	private $_languages = array();
	private $_language = '';
	private $_lang = array();
	private $_fang = array();
	private $input;

	public function __construct() {
		$this->tsn = getInstance();
		$this->input = $this->tsn->input;
		$this->loadFallback();
		$languages = glob($this->_languages_path.'*.php');

		foreach($languages as $language) {
			array_push($this->_languages, str_replace('.php', '', str_replace($this->_languages_path, '', $language)));
		}

		if(is_null($this->input->get('lang'))) {
			if(isset($_SESSION['LANG']))
				$this->setLanguage($_SESSION['LANG']);
			else
				$this->setLanguage($this->detectLanguage());
		} else {
			$_SESSION['LANG'] = $this->input->get('lang');
			$this->setLanguage($_SESSION['LANG']);
		}
	}

	public function get($key) {
		return array_key_exists(strtolower($key), $this->_lang) ? $this->_lang[strtolower($key)] : (array_key_exists(strtolower($key), $this->_fang) ? $this->_fang[strtolower($key)] : $key);
	}

	public function setLanguage($lang) {
		if(in_array($lang, $this->_languages))
			$this->_language = strtolower($lang);
		else
			$this->_language = 'en';

		$this->loadLanguage();
	}

	public function getLanguages() {
		return $this->_languages;
	}

	private function loadFallback() {
		$file = $this->_languages_path.'en.php';
		include $file;

		$this->_fang = $lang;
	}

	private function detectLanguage() {
		return substr($this->input->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
	}

	private function loadLanguage() {
		$file = $this->_languages_path.$this->_language.'.php';

		if(!file_exists($file)) $file = str_replace($file, $this->tsn->config['defaultLanguage'], $file);

		include $file;

		$this->_lang = $lang;
	}
}
