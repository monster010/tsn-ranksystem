<?php
class TSN_Router {
	private $input;
	private $defController;
	private $defMethod;

	public function __construct() {
		$tsn = getInstance();
		$this->load = $tsn->load;
		$this->input = $tsn->input;

		$this->load->helper('errors');

		$route = explode('/', DEFAULT_ROUTE);
		$this->defController = ucfirst($route[0]);
		$this->defMethod = $route[1];
	}

	public function handle() {
		$request = ltrim($this->input->server('PATH_INFO'), '/');
		$request = rtrim($request, '/');
		$parts = explode('/', $request);

		if(empty($parts[0])) {
			require_once APP_PATH.'controllers/'.$this->defController.'.php';

			$controller = new $this->defController();
			$controller->{$this->defMethod}();
		} else {
			$parts[0] = ucfirst($parts[0]);
			$file = APP_PATH.'controllers/'.$parts[0].'.php';

			if(!file_exists($file)) show_404();

			require_once $file;

			$controller = new $parts[0]();
			unset($parts[0]);

			$method = (isset($parts[1]) ? $parts[1] : 'view');
			unset($parts[1]);

			if(method_exists($controller, $method)) {
				if(count($parts) == 0) $controller->{$method}();
				else call_user_func_array(array($controller, $method), $parts);
			} else {
				show_404();
			}
		}
	}
}
