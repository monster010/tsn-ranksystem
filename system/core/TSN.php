<?php
const TSN_VERSION = 'MVC';

class TSN {
	protected static $_instance;
	protected $_loaded_classes = array();
	protected $_tsn_keys = array();
	private $_tsn_vars = array();

	public $config = array('defaultLanguage' => 'en');

	private function __construct() {
		$this->load = new TSN_Loader();
		$this->input = new TSN_Input();
		$this->db = new TSN_Database();
		self::$_instance = $this;
	}

	public function __set($var, $val) {
		$this->_tsn_vars[$var] = $val;
	}

	public function __get($var) {
		if(isset($this->_tsn_vars[$var]))
			return $this->_tsn_vars[$var];

		throw new Exception($var.' doen\'t exists');
	}

	public static function getInstance() {
		if(!self::$_instance)
			self::$_instance  = new self();

		return self::$_instance;
	}

	public function is_loaded($class) {
		return array_search(ucfirst($class), $this->_loaded_classes, TRUE);
	}
}
