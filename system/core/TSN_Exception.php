<?php
class TSN_Exception {
	public $ob_level;
	public $levels = array(
		E_ERROR				=> 'Error',
		E_WARNING			=> 'Warning',
		E_PARSE				=> 'Parsing Error',
		E_NOTICE			=> 'Notice',
		E_CORE_ERROR		=> 'Core Error',
		E_CORE_WARNING		=> 'Core Warning',
		E_COMPILE_ERROR		=> 'Compile Error',
		E_COMPILE_WARNING	=> 'Compile Warning',
		E_USER_ERROR		=> 'User Error',
		E_USER_WARNING		=> 'User Warning',
		E_USER_NOTICE		=> 'User Notice',
		E_STRICT			=> 'Runtime Notice'
	);
	
	public function __construct() {
		$this->ob_level = ob_get_level();
	}
	
	public function show_404() {
		echo $this->show_error('404 Page not Found', 'The page you requested was not found!', 'error_404', 404);
		exit(4);
	}
	
	public function show_error($heading, $message, $template = 'error_general', $code = 500) {
		$path = APP_PATH.'views/_errors/';
		
		set_status_header($code);
		$message = '<p>'.(is_array($message) ? implode('</p><p>', $message) : $message).'</p>';
		
		if(ob_get_level() > $this->ob_level + 1) {
			ob_end_flush();
		}
		
		ob_start();
		include $path.$template.'.php';
		$buffer = ob_get_contents();
		ob_end_clean();
		
		return $buffer;
	}
}