<?php
function outLangStr($key) {
	global $lang;
	return $lang->get($key);
}

function outLangList() {
	global $lang;
	$out = '';

	foreach($lang->getLanguages() as $langkey) {
		$out .= '<li><a href="?lang='.$langkey.'"><span class="flag-icon flag-icon-'.$langkey.'"></span> '.$lang->get($langkey).'</a></li>';
	}

	echo $out;
}

function outLangOption() {
	global $lang;
	$out = '';

	foreach($lang->getLanguages() as $langkey) {
		$out .= '<option value="'.$langkey.'">'.$lang->get($langkey).'</option>';
	}

	echo $out;
}
