<?php

function human_readable_size($bytes) {
	$size = array(' B', ' KiB', ' MiB', ' GiB', ' TiB', ' PiB', ' EiB', ' ZiB', ' YiB');
	$factor = floor((strlen($bytes) - 1) / 3);
	return sprintf("%.2f", ($bytes / pow(1024, $factor)).@$size[$factor]);
}
