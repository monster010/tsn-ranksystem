<?php
function gen_error($msg, $type = null) {
	switch($type) {
		case 1:
		case 'info':
			$type = 'info';
			break;
		case 2:
		case 'warning':
			$type = 'warning';
			break;
		default:
		case 3:
		case 'error':
			$type = 'danger';
			break;
		case 4:
		case 'success':
			$type = 'success';
			break;
	}

	return "<div class=\"alert alert-$type alert-dismissible\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><i class=\"fa fa-times\"></i></button>$msg</div>";
}

function show_404() {
	$err = new TSN_Exception();
	$err->show_404();
	exit(4);
}

function show_error($message, $code = 500, $heading = 'An Error was Encountered') {
	$code = abs($code);

	if($code < 100) {
		$exit = $code + 9;
		$code = 500;
	} else {
		$exit = 1;
	}

	$err = new TSN_Exception();
	echo $err->show_error($heading, $message, 'error_general', $code);
	exit($exit);
}

function set_status_header($code, $text = '') {
	if(empty($code) || !is_numeric($code))
		show_error('Status codes must be numeric!', 500);

	if(empty($text)) {
		is_int($code) || $code = (int)$code;

		$stati = array(
			100	=> 'Continue',
			101	=> 'Switching Protocols',

			200	=> 'OK',
			201	=> 'Created',
			202	=> 'Accepted',
			203	=> 'Non-Authoritative Information',
			204	=> 'No Content',
			205	=> 'Reset Content',
			206	=> 'Partial Content',

			300	=> 'Multiple Choices',
			301	=> 'Moved Permanently',
			302	=> 'Found',
			303	=> 'See Other',
			304	=> 'Not Modified',
			305	=> 'Use Proxy',
			307	=> 'Temporary Redirect',

			400	=> 'Bad Request',
			401	=> 'Unauthorized',
			402	=> 'Payment Required',
			403	=> 'Forbidden',
			404	=> 'Not Found',
			405	=> 'Method Not Allowed',
			406	=> 'Not Acceptable',
			407	=> 'Proxy Authentication Required',
			408	=> 'Request Timeout',
			409	=> 'Conflict',
			410	=> 'Gone',
			411	=> 'Length Required',
			412	=> 'Precondition Failed',
			413	=> 'Request Entity Too Large',
			414	=> 'Request-URI Too Long',
			415	=> 'Unsupported Media Type',
			416	=> 'Requested Range Not Satisfiable',
			417	=> 'Expectation Failed',
			422	=> 'Unprocessable Entity',
			426	=> 'Upgrade Required',
			428	=> 'Precondition Required',
			429	=> 'Too Many Requests',
			431	=> 'Request Header Fields Too Large',

			500	=> 'Internal Server Error',
			501	=> 'Not Implemented',
			502	=> 'Bad Gateway',
			503	=> 'Service Unavailable',
			504	=> 'Gateway Timeout',
			505	=> 'HTTP Version Not Supported',
			511	=> 'Network Authentication Required'
		);

		if(isset($stati[$code]))
			$text = $stati[$code];
		else
			show_error('No status text available. Place check your status code number or supply your own message text.', 500);
	}

	$proto = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';
	header($proto.' '.$code.' '.$text, TRUE, $code);
}
