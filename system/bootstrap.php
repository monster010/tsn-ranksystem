<?php
/**
 * Define Global Variables
 */
define('DEFAULT_ROUTE', 'stats/server');


/**
 * Define Paths
 */
define('ROOT_PATH', dirname(dirname(__FILE__)).'/');
define('SYSTEM_PATH', ROOT_PATH.'system/');
define('APP_PATH', ROOT_PATH.'app/');

require_once SYSTEM_PATH.'core/TSN.php';
require_once SYSTEM_PATH.'core/Loader.php';

function getInstance() {
	return TSN::getInstance();
}

$tsn = getInstance();

$lang = new TSN_Lang();
$tsn->load->helper(array('lang', 'errors'));
