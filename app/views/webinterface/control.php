<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wibot4'); ?></h1>
		</div>
	</div>
	<form class="form-horizontal" name="control" method="post">
		<div class="form-group">
			<button type="submit" class="btn btn-success center-block"><i class="fa fa-fw fa-power-off"></i> <?php outLangStr('wibot5'); ?></button>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-warning center-block"><i class="fa fa-fw fa-refresh"></i> <?php outLangStr('wibot7'); ?></button>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-danger center-block"><i class="fa fa-fw fa-times"></i> <?php outLangStr('wibot6'); ?></button>
		</div>
	</form>
	<hr>
	<div class="row">
		<div class="col-lg-2">
			<h4><?php outLangStr('wibot8'); ?></h4>
		</div>
		<form class="form-horizontal" name="logfilter" method="post">
			<div class="col-lg-2">
				<div class="col-sm-12">
					<input type="text" class="form-control" name="logfilter[]" value="" data-switch-no-init onchange="this.form.submit();">
				</div>
			</div>
			<div class="col-lg-1">
				<div class="checkbox">
					<label><input id="switch-create-destroy" type="checkbox" name="logfilter[]" value="critical" data-switch-no-init onchange="this.form.submit();">Critical</label>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="checkbox">
					<label><input id="switch-create-destroy" type="checkbox" name="logfilter[]" value="error" data-switch-no-init onchange="this.form.submit();">Error</label>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="checkbox">
					<label><input id="switch-create-destroy" type="checkbox" name="logfilter[]" value="warning" data-switch-no-init onchange="this.form.submit();">Warning</label>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="checkbox">
					<label><input id="switch-create-destroy" type="checkbox" name="logfilter[]" value="notice" data-switch-no-init onchange="this.form.submit();">Notice</label>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="checkbox">
					<label><input id="switch-create-destroy" type="checkbox" name="logfilter[]" value="info" data-switch-no-init onchange="this.form.submit();">Info</label>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="checkbox">
					<label><input id="switch-create-destroy" type="checkbox" name="logfilter[]" value="debug" data-switch-no-init onchange="this.form.submit();">Debug</label>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="col-sm-8 pull-left">
					<select class="selectpicker show-tick form-control" id="number" name="number" onchange="this.form.submit();">
						<option value="20">20</option>
						<option value="50">50</option>
						<option value="100">100</option>
						<option value="200">200</option>
						<option value="500">500</option>
						<option value="9999">9999</option>
					</select>
				</div>
			</div>
		</form>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<pre>LOG</pre>
		</div>
	</div>
</div>