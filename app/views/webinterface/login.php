<div class="container-fluid">
	<div id="login-overlay" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?php outLangStr('isntwiusrh'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<form id="loginForm" method="post">
							<div class="form-group">
								<label for="username" class="control-label"><?php outLangStr('user'); ?></label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
									<input type="text" class="form-control" name="username" placeholder="<?php outLangStr('user'); ?>" maxlength="64" autofocus>
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="control-label"><?php outLangStr('pass'); ?></label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
									<input type="password" class="form-control" name="password" placeholder="<?php outLangStr('pass'); ?>">
								</div>
							</div>
							<p><button type="submit" class="btn btn-success btn-block"><?php outLangStr('login'); ?></button></p>
							<p class="small text-right"><a href=""><?php outLangStr('pass5'); ?></a></p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>