<div class="container-fluid">
	<div class="row">
		<div class="page-header">
			<h1 class="page-header"><?php outLangStr('wihlvs'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" data-toggle="validator" name="update" method="post">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('witime'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<select class="selectpicker show-tick form-control" data-live-search="true" name="timezone">
							<?php
							$timezonear = DateTimeZone::listIdentifiers();
							
							foreach($timezonear as $timez) {
								echo "<option value=\"$timez\">$timez</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('widaform'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="dateformat" value="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wilog'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8 required-field-block">
						<input type="text" class="form-control" data-pattern=".*(\/|\\)$" data-error="The Logpath must end with / or \" name="logpath" value="" required>
						<div class="help-block with-errors"></div>
						<div class="required-icon"><div class="text">*</div></div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wilang'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<select class="selectpicker show-tick form-control" name="languagedb">
							<?php outLangOption(); ?>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiadmuuid'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8 required-field-block">
						<input type="text" class="form-control" data-pattern="^([A-Za-z0-9\\\/\+]{27}=)$" data-error="Check the entered unique ID!" name="adminuuid" value="" required>
						<div class="help-block with-errors"></div>
						<div class="required-icon"><div class="text">*</div></div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiupcheck'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input class="switch-animate" type="checkbox" checked data-size="mini" name="upcheck" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiuptime'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="updateinfotime" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiupuid'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" data-pattern="^([A-Za-z0-9\\\/\+]{27}=,)*([A-Za-z0-9\\\/\+]{27}=)$" data-error="Check all unique IDs are correct and your list do not ends with a comma!" rows="1" name="uniqueid" maxlength="500"></textarea>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>

<script type="text/javascript">
$('input[name="updateinfotime"]').TouchSpin({
	min: 0,
	max: 86400,
	verticalbuttons: true,
	prefix: 'Sec.:'
});
</script>