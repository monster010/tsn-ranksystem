<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wihlmsg'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" name="update" method="post">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wimsgusr'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input class="switch-animate" type="checkbox" checked data-size="mini" name="msgtouser" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wimsgmsg'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="5" name="rankupmsg" maxlength="500"></textarea>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wimsgsn'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="15" name="servernews" maxlength="5000"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('winxinfo'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<select class="selectpicker show-tick form-control" id="basic" name="nextupinfo">
									<option value="0"><?php outLangStr('winxmode1'); ?></option>
									<option value="1"><?php outLangStr('winxmode2'); ?></option>
									<option value="2"><?php outLangStr('winxmode3'); ?></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('winxmsg1'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="5" name="nextupinfomsg1" maxlength="500"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('winxmsg2'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="5" name="nextupinfomsg2" maxlength="500"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('winxmsg3'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="5" name="nextupinfomsg3" maxlength="500"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>