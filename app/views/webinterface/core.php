<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wihlcfg'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" data-toggle="validator" name="update" method="post">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wisupidle'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<select class="selectpicker show-tick form-control" id="basic" name="substridle">
							<option value="0"><?php outLangStr('wishcolot'); ?></option>
							<option value="1"><?php outLangStr('wishcolat'); ?></option>
						</select>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiexres'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<select class="selectpicker show-tick form-control" id="basic" name="resetexcept">
									<option value="0"><?php outLangStr('wiexres1'); ?></option>
									<option value="1"><?php outLangStr('wiexres2'); ?></option>
									<option value="2"><?php outLangStr('wiexres3'); ?></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiexuid'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" data-pattern="^([A-Za-z0-9\\\/\+]{27}=,)*([A-Za-z0-9\\\/\+]{27}=)$" data-error="Check all unique IDs are correct and your list do not ends with a comma!" rows="1" name="exceptuuid" maxlength="999"></textarea>
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiexgrp'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" data-pattern="^([0-9]+,)*[0-9]+$" data-error="Only use digits separated with a comma! Also must the first and last value be digit!" rows="1" name="exceptgroup" maxlength="999"></textarea>
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiexcid'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<textarea class="form-control" data-pattern="^([0-9]+,)*[0-9]+$" data-error="Only use digits separated with a comma! Also must the first and last value be digit!" rows="1" name="exceptcid" maxlength="999"></textarea>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group required-field-block">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wigrptime'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<textarea class="form-control" data-pattern="^([0-9]+=>[0-9]+,)*[0-9]+=>[0-9]+$" data-error="Wrong definition, please look at description for more details. No comma at ending!" rows="5" name="grouptime" maxlength="5000" required></textarea>
						<div class="required-icon"><div class="text">*</div></div>
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiignidle'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="ignoreidle" value="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wichdbid'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input class="switch-animate" type="checkbox" checked data-size="mini" name="resetbydbchange" value="">
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('cleanc'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input class="switch-animate" type="checkbox" checked data-size="mini" name="cleanclients" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('cleanp'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="cleanperiod" value="">
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiboost'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<textarea class="form-control" data-pattern="^([1-9][0-9]{0,9}=>[0-9]{0,9}=>[1-9][0-9]{0,9},)*[1-9][0-9]{0,9}=>[0-9]{0,9}=>[1-9][0-9]{0,9}$" data-error="Wrong definition, please look at description for more details. No comma at ending!" rows="5" name="boost" maxlength="999"></textarea>
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>

<script type="text/javascript">
$('input[name="ignoreidle"]').TouchSpin({
	min: 0,
	max: 65535,
	verticalbuttons: true,
	prefix: 'Sec.:'
});
$('input[name="cleanperiod"]').TouchSpin({
	min: 0,
	max: 9223372036854775807,
	verticalbuttons: true,
	prefix: 'Sec.:'
});
</script>