<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('stag0001'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" data-toggle="validator" name="update" method="post">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('stag0013'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input class="switch-animate" type="checkbox" checked data-size="mini" name="assign-groups-active" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('stag0002'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<textarea class="form-control" data-pattern="^([0-9]+,)*[0-9]+$" data-error="Wrong definition, please look at description for more details. No comma at ending!" rows="5" name="assign_groups_groupids" maxlength="5000" required></textarea>
								<div class="required-icon"><div class="text">*</div></div>
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('stag0004'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="assign-groups-limit" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>

<script type="text/javascript">
$('input[name="assign-groups-limit"]').TouchSpin({
	min: 1,
	max: 65534,
	verticalbuttons: true,
	prefix: 'No.'
});
</script>