<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wihlts'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" name="update" method="post">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#isntwiDB"><?php outLangStr('isntwidbtype'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<p class="form-control-static">MySQL 3.x/4.x/5.x [recommend]</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#isntwiDB"><?php outLangStr('isntwidbhost'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="dbhost" value="" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#isntwiDB"><?php outLangStr('isntwidbname'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="dbname" value="" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#isntwiDB"><?php outLangStr('isntwidbusr'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="dbuser" value="" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#isntwiDB"><?php outLangStr('isntwidbpass'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="password" class="form-control" name="dbpass" value="" data-toggle="password" data-placement="before" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>