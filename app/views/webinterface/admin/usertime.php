<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wihladm1'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" method="post" name="usertime">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiadmhide'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 pull-right">
								<select class="selectpicker show-tick form-control" id="number" name="number" onchange="this.form.submit();">
									<option value="yes">hide</option>
									<option calue="no">show</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wiselcld'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<select class="selectpicker show-tick form-control" data-live-search="true" multiple name="user[]">
									
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('setontime'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="setontime_day">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="setontime_hour">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="setontime_min">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="setontime_sec">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>

<script type="text/javascript">
$('input[name="setontime_day"]').TouchSpin({
	min: 0,
	max: 106751991167299,
	verticalbuttons: true,
	prefix: 'Day(s):'
});
$('input[name="setontime_hour"]').TouchSpin({
	min: 0,
	max: 23,
	verticalbuttons: true,
	prefix: 'Hour(s):'
});
$('input[name="setontime_min"]').TouchSpin({
	min: 0,
	max: 59,
	verticalbuttons: true,
	prefix: 'Min.:'
});
$('input[name="setontime_sec"]').TouchSpin({
	min: 0,
	max: 59,
	verticalbuttons: true,
	prefix: 'Sec.:'
});
</script>