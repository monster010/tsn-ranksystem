<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wihlts'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" name="update" method="post">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3host'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="tshost" value="" maxlength="64" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3query'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block-spin">
								<input type="text" class="form-control" name="tsquery" value="" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3voice'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block-spin">
								<input type="text" class="form-control" name="tsvoice" value="" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3querusr'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="tsuser" value="" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3querpw'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="password" class="form-control" name="tspass" id="tspass" value="" required data-toggle="password" data-placement="before">
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3qnm'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="queryname" value="" maxlength="30" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3qnm2'); ?> <i class="help-hover fa fa-info"></i></label>
							<div class="col-sm-8 required-field-block">
								<input type="text" class="form-control" name="queryname2" value="" maxlength="30" required>
								<div class="required-icon"><div class="text">*</div></div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3dch'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="defchid" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3sm'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<select class="selectpicker show-tick form-control" id="basic" name="slowmode">
							<option data-subtext="[recommended]" value="0">Realtime (deactivated)</option>
							<option data-divider="true"></option>
							<option data-subtext="(0,2 seconds)" value="200000">Low delay</option>
							<option data-subtext="(0,5 seconds)" value="500000">Middle delay</option>
							<option data-subtext="(1,0 seconds)" value="1000000">High delay</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#wits3Modal"><?php outLangStr('wits3avat'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="avatar_delay" value="" required>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>

<script type="text/javascript">
$('input[name="tsquery"]').TouchSpin({
	min: 0,
	max: 65535,
	verticalbuttons: true,
	prefix: 'TCP:'
});
$('input[name="tsvoice"]').TouchSpin({
	min: 0,
	max: 65535,
	verticalbuttons: true,
	prefix: 'UDP:'
});
$('input[name="defchid"]').TouchSpin({
	min: 0,
	max: 9223372036854775807,
	verticalbuttons: true,
	prefix: 'ID:'
});
$('input[name="avatar_delay"]').TouchSpin({
	min: 0,
	max: 65535,
	verticalbuttons: true,
	prefix: 'Sec.:'
});
</script>