<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('wihlsty'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" name="update" method="post">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolrg'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolrg" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolcld'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolcld" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcoluuid'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcoluuid" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcoldbid'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcoldbid" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolls'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolls" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolot'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolot" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolit'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolit" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolat'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolat" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolas'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolas" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolgs'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showgrpsince" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolnx'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolnx" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishcolsg'); ?> <i class="help-hover fa fa-info"></i></label>
						<div class="col-sm-8">
							<input class="switch-animate" type="checkbox" checked data-size="mini" name="showcolsg" value="">
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishexcld'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input class="switch-animate" type="checkbox" checked data-size="mini" name="showexcld" value="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishhicld'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input class="switch-animate" type="checkbox" checked data-size="mini" name="showhighest" value="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" data-toggle="modal" data-target="#helpModal"><?php outLangStr('wishnav'); ?> <i class="help-hover fa fa-info"></i></label>
					<div class="col-sm-8">
						<input class="switch-animate" type="checkbox" checked data-size="mini" name="shownav" value="">
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('wisvconf'); ?></button>
	</form>
</div>