<div class="container-fluid">
	<div id="login-overlay" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title" id="myModalLabel"><?php outLangStr('wichpw4'); ?> - <?php outLangStr('wi'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<form id="resetForm" method="POST">
							<div class="form-group">
								<label for="password" class="control-label"><?php outLangStr('pass3'); ?>:</label>
								<div class="input-group-justified">
									<input type="password" class="form-control" name="oldpwd" data-toggle="password" data-placement="before" placeholder="<?php outLangStr('pass3'); ?>">
								</div>
							</div>
							<p>&nbsp;</p>
							<div class="form-group">
								<label for="password" class="control-label"><?php outLangStr('pass4'); ?>:</label>
								<div class="input-group-justified">
									<input type="password" class="form-control" name="newpwd1" data-toggle="password" data-placement="before" placeholder="<?php outLangStr('pass4'); ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="control-label"><?php outLangStr('pass4'); ?> (<?php outLangStr('repeat'); ?>):</label>
								<div class="input-group-justified">
									<input type="password" class="form-control" name="newpwd2" data-toggle="password" data-placement="before" placeholder="<?php outLangStr('pass4'); ?> (<?php outLangStr('repeat'); ?>)">
								</div>
							</div>
							<br>
							<p>
								<button type="submit" class="btn btn-success btn-block" name="changepw"><?php outLangStr('wichpw4'); ?></button>
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>