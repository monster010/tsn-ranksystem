<div class="container-fluid">
	<div id="login-overlay" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title" id="myModalLabel"><?php outLangStr('wirtpw7'); ?> - <?php outLangStr('wi'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<form id="resetForm" method="POST">
							<p><?php outLangStr('wirtpw8'); ?></p>
							<p><?php outLangStr('wirtpw9'); ?>
								<ul>
									<li><?php outLangStr('wirtpw10'); ?></li>
									<li><?php outLangStr('wirtpw11'); ?></li>
									<li><?php outLangStr('wirtpw12'); ?></li>
								</ul>
							</p>
							<br>
							<p>
								<button type="submit" class="btn btn-success btn-block" name="resetpw"><?php outLangStr('wirtpw7'); ?></button>
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>