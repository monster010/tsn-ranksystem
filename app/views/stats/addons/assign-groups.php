<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('stag0001'); ?></h1>
		</div>
	</div>
	
	<form class="form-horizontal" method="post" name="update">
		<div class="row">
			<div class="col-md-3 col-md-offset-6">
				<p class="text-right"><strong><?php outLangStr('stag0011'); ?></strong></p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-5 control-label"></label>
							<label class="col-sm-1 control-label"><img src="/cache/icons/.png" alt="groupicon"></label>
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-2">
								<input class="switch-animate" type="checkbox" checked data-size="mini" name="" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary center-block" name="update"><?php outLangStr('stag0012'); ?></button>
	</form>
</div>