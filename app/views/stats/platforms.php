<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('stna0006'); ?> - <?php outLangStr('stna0002'); ?></h1>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th><?php outLangStr('stna0006'); ?></th>
							<th><?php outLangStr('stix0060'); ?> <?php outLangStr('stna0004'); ?></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>