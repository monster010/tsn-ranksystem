<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('sttw0001'); ?> <small><?php outLangStr('sttw0002'); ?></small></h1>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-4 col-lg-offset-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#1st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-5x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-4 col-lg-offset-2">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#2st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-5x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#3st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-5x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-4">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#4st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#5st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#6st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#7st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#8st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#9st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<p class="text-center"><i>#10st</i></p>
							<p class="text-center"><i class="fa fa-trophy fa-2x"></i></p>
						</div>
						<div class="col-xs-9 text-right">
							<div class="tophuge"><span>Name</span></div>
							<div>----</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>