<div class="container-fluid">
	<table class="table table-striped">
		<thead data-spy="affix" data-offset-top="100">
			<tr class="text-center">
				<th><a href=""><?php outLangStr('listrank'); ?></a></th>
				<th><a href=""><?php outLangStr('listnick'); ?></a></th>
				<th><a href=""><?php outLangStr('listuid'); ?></a></th>
				<th><a href=""><?php outLangStr('listcldbid'); ?></a></th>
				<th><a href=""><?php outLangStr('listip'); ?></a></th>
				<th><a href=""><?php outLangStr('listseen'); ?></a></th>
				<th><a href=""><?php outLangStr('listsumo'); ?></a></th>
				<th><a href=""><?php outLangStr('listsumi'); ?></a></th>
				<th><a href=""><?php outLangStr('listsuma'); ?></a></th>
				<th><a href=""><?php outLangStr('listacsg'); ?></a></th>
				<th><a href=""><?php outLangStr('listgrps'); ?></a></th>
				<th><a href=""><?php outLangStr('listnxup'); ?></a></th>
				<th><a href=""><?php outLangStr('listnxsg'); ?></a></th>
			</tr>
		</thead>
		<tbody>
			
		</tbody>
	</table>
</div>

<script type="text/javascript">
	$("th").each(function() {
		$(this).width($(this).width());
	});
</script>