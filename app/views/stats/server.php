<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				<?=outLangStr('stix0001')?> <small><a href="#infoModal" data-toggle="modal" class="text-muted"><i class="fa fa-info"></i></a></small>
			</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$server['total_user']?></div>
							<div><?=outLangStr('stix0002')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/list">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0003')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-clock-o fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=sprintf(outLangStr('days'), round(($server['total_online_time'] / 86400)))?></div>
							<div><?=outLangStr('stix0004')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/top/all">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0005')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-clock-o fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=sprintf(outLangStr('days'), round(($server['total_online_month'] / 86400)))?></div>
							<div><?=outLangStr('stix0049')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/top/month">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0006')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-clock-o fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=sprintf(outLangStr('days'), round(($server['total_online_week'] / 86400)))?></div>
							<div><?=outLangStr('stix0050')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/top/week">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0007')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-9">
							<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?=outLangStr('stix0008')?> <i><?=outLangStr('stix0009')?></i></h3>
						</div>
						<div class="col-xs-3">
							<div class="btn-group dropup pull-right">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=outLangStr('stix0012')?> <i class="fa fa-angle-down"></i></button>

								<ul class="dropdown-menu">
									<li><a href=""><?=outLangStr('stix0013')?></a></li>
									<li><a href=""><?=outLangStr('stix0014')?></a></li>
									<li><a href=""><?=outLangStr('stix0015')?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div id="server-usage-chart"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-3">
			<div class="panel panel-primary">
				<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> <?=outLangStr('stix0016')?></h3></div>
				<div class="panel-body">
					<div id="time-gap-donut"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-green">
				<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> <?=outLangStr('stix0017')?></h3></div>
				<div class="panel-body">
					<div id="client-version-donut"></div>
				</div>
				<a href="/stats/versions">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0061')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-yellow">
				<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> <?=outLangStr('stix0018')?></h3></div>
				<div class="panel-body">
					<div id="client-version-donut"></div>
				</div>
				<a href="/stats/nations">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0062')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-red">
				<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> <?=outLangStr('stix0019')?></h3></div>
				<div class="panel-body">
					<div id="user-platform-donut"></div>
				</div>
				<a href="/stats/platforms">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0063')?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$server['user_today']?></div>
							<div><?=outLangStr('stix0060')?> <?=outLangStr('stix0055')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/list">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0059')?> (<?=outLangStr('stix0055')?>)</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$server['user_week']?></div>
							<div><?=outLangStr('stix0060')?> <?=sprintf(outLangStr('stix0056'), '7')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/list">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0059')?> (<?=sprintf(outLangStr('stix0056'), '7')?>)</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$server['user_month']?></div>
							<div><?=outLangStr('stix0060')?> <?=sprintf(outLangStr('stix0056'), '30')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/list">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0059')?> (<?=sprintf(outLangStr('stix0056'), '30')?>)</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$server['user_quarter']?></div>
							<div><?=outLangStr('stix0060')?> <?=sprintf(outLangStr('stix0056'), '90')?></div>
						</div>
					</div>
				</div>
				<a href="/stats/list">
					<div class="panel-footer">
						<span class="pull-left"><?=outLangStr('stix0059')?> (<?=sprintf(outLangStr('stix0056'), '90')?>)</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<h2><?=outLangStr('stix0020')?></h2>
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<tbody>
						<tr>
							<th><?=outLangStr('stix0023')?></th>
							<td>
								<?php
									if($server['server_status'] == 1 || $server['server_'] == 3) {
										echo '<span class="text-success">'.outLangStr('stix0024').'</span>';
									} else {
										echo '<span class="text-success">'.outLangStr('stix0025').'</span>';
									}
								?>
							</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0026')?></th>
							<td>
								<?php
									if($server['server_status'] == 0) {
										echo '0';
									} else {
										echo $server['server_used_slots'].' / '.($server['server_used_slots'] + $server['server_free_slots']);
									}
								?>
							</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0027')?></th>
							<td><?=$server['server_channel_amount']?></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0028')?></th>
							<td>
								<?php
									if($server['server_status'] == 0) {
										echo '-';
									} else {
										echo $server['server_ping'].' ms';
									}
								?>
							</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0029')?></th>
							<td><?=human_readable_size($server['server_bytes_down'])?></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0030')?></th>
							<td><?=human_readable_size($server['server_bytes_up'])?></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0031')?></th>
							<td>
								<?php
									$serveruptime = new DateTime("@".$server['server_uptime']);

									if($server['server_status'] == 0) {
										echo '-&nbsp;&nbsp;&nbsp;(<i>'.outLangStr('stix0032').'&nbsp;'.(new DateTime("@0"))->diff($serveruptime)->format($timeformat).')</i>';
									} else {
										echo outLangStr('stix0033');
									}
								?>
							</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0034')?></th>
							<td>
								<?php
									if($server['server_status'] == 0) {
										echo '-';
									} else {
										echo ($server['server_packet_loss'] * 100).' %';
									}
								?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-lg-6">
			<h2><?=outLangStr('stix0035')?></h2>
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<tbody>
						<tr>
							<th><?=outLangStr('stix0036')?></th>
							<td><span class="text-success"><?=outLangStr('stix0024')?></span></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0037')?></th>
							<td>0 / 0</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0038')?></th>
							<td>
								<?php
									if($server['server_pass'] == '0') {
										echo outLangStr('stix0039');
									} else {
										echo outLangStr('stix0040');
									}
								?>
							</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0041')?></th>
							<td><?=$server['server_id']?></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0042')?></th>
							<td><?=$server['server_platform']?></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0043')?></th>
							<td><?=substr($server['server_version'], 0, strpos($server['server_version'], ' '))?></td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0044')?></th>
							<td>
								<?php
									if($server['server_creation_date'] == 0) {
										echo outLangStr('stix0051');
									} else {
										echo date('d/m/Y', $server['server_creation_date']);
									}
								?>
							</td>
						</tr>
						<tr>
							<th><?=outLangStr('stix0045')?></th>
							<td>
								<?php
									if ($server['server_weblist'] == 1) {
										echo '<a href="https://www.planetteamspeak.com/serverlist/result/server/ip/';

										if($ts['host']=='localhost' || $ts['host']=='127.0.0.1') {
											echo $_SERVER['HTTP_HOST'];
										} else {
											echo $ts['host'];
										}

										echo ':'.$ts['voice'] .'" target="_blank">'.outLangStr('stix0046').'</a>';
									} else {
										echo outLangStr('stix0047');
									}
								?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var daysLabel = document.getElementById("days");
	var hoursLabel = document.getElementById("hours");
	var minutesLabel = document.getElementById("minutes");
	var secondsLabel = document.getElementById("seconds");
	var totalSeconds = 0;

	setInterval(setTime, 1000);

	function setTime() {
		++totalSeconds;
		secondsLabel.innerHTML = pad(totalSeconds%60);
		minutesLabel.innerHTML = pad(parseInt(totalSeconds/60)%60);
		hoursLabel.innerHTML = pad(parseInt(totalSeconds/3600)%24)
		daysLabel.innerHTML = pad(parseInt(totalSeconds/86400))
	}

	function pad(val) {
		var valString = val + "";
		if(valString.length < 2) {
			return "0" + valString;
		} else {
			return valString;
		}
	}

	Morris.Donut({
		element: 'time-gap-donut',
		data: [
			{label: "<?=outLangStr('stix0053')?>", value: <?=round(($server['total_active_time'] / 86400))?>},
			{label: "<?=outLangStr('stix0054')?>", value: <?=round(($server['total_inactive_time'] / 86400))?>}
		]
	});

	Morris.Donut({
		element: 'client-version-donut',
		data: [
			{label: "<?=$server['version_name_1']?>", value: <?=$server['version_1']?>},
			{label: "<?=$server['version_name_2']?>", value: <?=$server['version_2']?>},
			{label: "<?=$server['version_name_3']?>", value: <?=$server['version_3']?>},
			{label: "<?=$server['version_name_4']?>", value: <?=$server['version_4']?>},
			{label: "<?=$server['version_name_5']?>", value: <?=$server['version_5']?>},
			{label: "<?=outLangStr('stix0052')?>", value: <?=$server['version_other']?>}
		],
		colors: [
			'#5cb85c',
			'#73C773',
			'#8DD68D',
			'#AAE6AA',
			'#C9F5C9',
			'#E6FFE6'
		]
	});

	Morris.Area({
		element: 'server-usage-chart',
		data: [
			<?php
				$chart_data = '';

				foreach($sv_usage as $chart_value) {
					$chart_time = date('Y-m-d H:i', $chart_value['timestamp']);
					$channel = $chart_value['channel'] - $chart_value['clients'];
					$chart_data .= '{ y: \''.$chart_time.'\', a: '.$chart_value['clients'].', b: '.$channel.', c: '. $chart_value['channel'].' }, ';
				}

				$chart_data = substr($chart_data, 0, -2);
				echo $chart_data;
			 ?>
		],
		xkey: 'y',
		ykeys: ['a', 'b'],
		hideHover: 'auto',
		hoverCallback:
			function (index, options, content, row) {
				return "<b>" + row.y + "</b><br><div class='morris-hover-point' style='color:#2677B5'>Clients: " + row.a + "</div><div class='morris-hover-point' style='color:#868F96'>Channel: " + (row.b + row.a) + "</div>";
			} ,
		labels: ['Clients', 'Channel']
	});

	Morris.Donut({
		element: 'user-platform-donut',
		data: [
			{label: "Windows", value: <?=$server['platform_1']?>},
			{label: "Linux", value: <?=$server['platform_3']?>},
			{label: "Android", value: <?=$server['platform_4']?>},
			{label: "iOS", value: <?=$server['platform_2']?>},
			{label: "OS X", value: <?=$server['platform_5']?>},
			{label: "<?=outLangStr('stix0052')?>", value: <?=$server['platform_other']?>}
		],
		colors: [
			'#d9534f',
			'#FF4040',
			'#FF5050',
			'#FF6060',
			'#FF7070',
			'#FF8080'
		]
	});

	/*Morris.Donut({
		element: 'user-descent-donut', data: [

		],
		colors: [
			'#f0ad4e',
			'#ffc675',
			'#fecf8d',
			'#ffdfb1',
			'#fce8cb',
			'#fdf3e5'
		]
	});*/
</script>
