<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php outLangStr('stri0001'); ?></h1>
		
			<h4 class="text-info"><strong><?php outLangStr('stri0002'); ?></strong></h4>
			<p><?php outLangStr('stri0003'); ?></p>
			<hr>
			
			<h4 class="text-success"><strong><?php outLangStr('stri0004'); ?></strong></h4>
			<p>The <a href="//ts-n.net/ranksystem.php" target="_blank">Ranksystem</a> was coded by <strong>Newcomer1989</strong>.</p>
			<p>Copyright &copy; 2009 - <?php echo date('Y'); ?> <a href="//ts-n.net" target="_blank">TeamSpeak Sponsoring TS-N.NET</a></p>
			<hr>
			
			<h4 class="text-warning"><strong><?php outLangStr('stri0005'); ?></strong></h4>
			<p><?php outLangStr('stri0006'); ?></p>
			<p><?php outLangStr('stri0007'); ?></p>
			<p><?php outLangStr('stri0008'); ?></p>
			<hr>
			
			<h4 class="text-danger"><strong><?php outLangStr('stri0009'); ?></strong></h4>
			<p><?php outLangStr('stri0010'); ?></p>
			<p><a href="//php.net" target="_blank">PHP</a> - Copyright &copy; 2001 - <?php echo date('Y'); ?> the <a href="//secure.php.net/credits.php" target="_blank">PHP Group</a></p><br>
			<p><?php outLangStr('stri0011'); ?></p>
			<p><a href="//jquery.com/" target="_blank">jQuery v3.1.1</a> - Copyright &copy; 2016 The jQuery Foundation</p> 
			<p><a href="//fontawesome.io" target="_blank">Font Awesome 4.7.0</a> - Copyright &copy; davegandy</p>
			<p><a href="//flag-icon-css.lip.is/" target="_blank">flag-icon-css 2.8.0</a> - Copyright &copy; 2016 lipis</p>
			<p><a href="//planetteamspeak.com/" target="_blank">TeamSpeak 3 PHP Framework 1.1.24</a> - Copyright &copy; 2010 Planet TeamSpeak</p> 
			<p><a href="//getbootstrap.com/" target="_blank">Bootstrap 3.3.7</a> - Copyright &copy; 2011-2016 Twitter, Inc.</p>
			<p><a href="//morrisjs.github.io/morris.js/" target="_blank">morris.js 0.5.0</a> - Copyright &copy; 2013 Olly Smith</p>
			<p><a href="//raphaeljs.com" target="_blank">Rapha&euml;l 2.2.1 - JavaScript Vector Library</a> - Copyright &copy; 2008-2012 Dmitry Baranovskiy</p>
			<p><a href="//startbootstrap.com" target="_blank">SB Admin Bootstrap Admin Template</a> - Copyright &copy; 2013-2016 Blackrock Digital LLC.</p>
			<p><a href="//www.bootstrap-switch.org" target="_blank">Bootstrap Switch 3.3.2</a> - Copyright &copy; 2013-2015 Mattia Larentis</p>
			<p><a href="//www.virtuosoft.eu/code/bootstrap-touchspin" target="_blank">Bootstrap TouchSpin 3.1.2</a> - Copyright &copy; 2013-2016 István Ujj-Mészáros</p>
			<p><a href="//silviomoreto.github.io/bootstrap-select" target="_blank">bootstrap-select 1.11.2</a> - Copyright &copy; 2013-2015 Silvio Moreto a.o.</p>
			<p><a href="//wenzhixin.net.cn/" target="_blank">Bootstrap Show Password 1.0.3</a> - Copyright &copy; 2014 zhixin wen</p>
			<hr>
			
			<h4 class="text-info"><strong><?php outLangStr('stri0012'); ?></strong></h4>
			<p><?php outLangStr('stri0013'); ?></p>
			<p><?php outLangStr('stri0014'); ?></p>
			<p><?php outLangStr('stri0015'); ?></p>
			<p><?php outLangStr('stri0016'); ?></p>
			<p><?php outLangStr('stri0017'); ?></p>
			<p><?php outLangStr('stri0018'); ?></p>
			<p><?php outLangStr('stri0019'); ?></p>
		</div>
	</div>
</div>