<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="version" content="<?php echo TSN_VERSION; ?>">
		<link rel="icon" href="/assets/img/icons/rs.png">
		
		<title>TS-N.NET Ranksystem</title>
		
		<link href="/assets/css/combined_wi.css?v=<?php echo TSN_VERSION; ?>" rel="stylesheet">
		<script src="/assets/js/combined_wi.js?v=<?php echo TSN_VERSION; ?>" type="text/javascript"></script>
		
		<script type="text/javascript">
		$(function() {
			$('.required-icon').tooltip({
				placement: 'left',
				title: 'Required field'
			});
			
			$('#password').password().on('show.bs.password', function(e) {
				$('#eventLog').text('On show event');
				$('#methods').prop('checked', true);
			}).on('hide.bs.password', function(e) {
				$('#eventLog').text('On hide event');
				$('#methods').prop('checked', false);
			});
			
			$('#methods').click(function() {
				$('#password').password('toggle');
			});
		});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-header">
					<a class="navbar-brand" href="/webinterface/">TS-N.NET Ranksystem - Webinterface <small><?php echo TSN_VERSION; ?></small></a>
				</div>
				
				<ul class="nav navbar-right top-nav">
					<li><a href="/"><i class="fa fa-bar-chart"></i> <?php outLangStr('winav6'); ?></a></li>
					<li><a href="/webinterface/chpassword"><i class="fa fa-lock"></i> <?php outLangStr('pass2'); ?></a></li>
					<li style="margin: 0 5px;"><button type="button" class="btn btn-primary navbar-btn"><i class="fa fa-sign-out"></i> <?php outLangStr('wilogout'); ?></button></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i> <i class="fa fa-angle-down"></i></a>
						
						<ul class="dropdown-menu">
							<?php outLangList(); ?>
						</ul>
					</li>
				</ul>
				
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav side-nav">
						<li><a href="/webinterface/teamspeak"><i class="fa fa-fw fa-headphones"></i> <?php outLangStr('winav1'); ?></a></li>
						<li><a href="/webinterface/database"><i class="fa fa-fw fa-database"></i> <?php outLangStr('winav2'); ?></a></li>
						<li><a href="/webinterface/core"><i class="fa fa-fw fa-cogs"></i> <?php outLangStr('winav3'); ?></a></li>
						<li><a href="/webinterface/others"><i class="fa fa-fw fa-wrench"></i> <?php outLangStr('winav4'); ?></a></li>
						<li><a href="/webinterface/notifys"><i class="fa fa-fw fa-envelope"></i> <?php outLangStr('winav5'); ?></a></li>
						<li><a href="/webinterface/stats"><i class="fa fa-fw fa-bar-chart"></i> <?php outLangStr('winav6'); ?></a></li>
						<li class="divider"></li>
						<li>
							<a href="#addons_collapse" data-toggle="collapse" data-taget="#addons_collapse"><i class="fa fa-fw fa-puzzle-piece"></i> <?php outLangStr('winav12'); ?><span class="pull-right"><i class="fa fa-angle-down fa-fw"></i></span></a>
							
							<ul id="addons_collapse" class="collapse">
								<li><a href="/webinterface/addons/assign-groups"><?php outLangStr('stag0001'); ?></a></li>
							</ul>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#users_collapse" data-toggle="collapse" data-taget="#users_collapse"><i class="fa fa-fw fa-users"></i> <?php outLangStr('winav7'); ?><span class="pull-right"><i class="fa fa-angle-down fa-fw"></i></span></a>
							
							<ul id="users_collapse" class="collapse">
								<li><a href="/webinterface/admin/usertime"><?php outLangStr('wihladm1'); ?></a></li>
							</ul>
						</li>
						<li class="divider"></li>
						<li><a href="/webinterface/control"><i class="fa fa-fw fa-power-off"></i> <?php outLangStr('winav8'); ?></a></li>
					</ul>
				</div>
			</nav>
			
			<div id="container">
				<div id="page-wrapper">