<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="version" content="<?=TSN_VERSION?>">
		<link rel="icon" href="/assets/img/icons/rs.png">

		<title>TS-N.NET Ranksystem</title>

		<link href="assets/css/combined_stats.css?v=<?=TSN_VERSION?>" rel="stylesheet">

		<script src="assets/js/combined_statistics.js?v=<?=TSN_VERSION?>" type="text/javascript"></script>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-header">
					<a class="navbar-brand" href="/">TS-N.NET Ranksystem</a>
				</div>

				<ul class="nav navbar-right top-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Benutzername <i class="fa fa-angle-down"></i></a>
					</li>
					<li style="margin: 0 5px;"><button type="button" class="btn btn-primary navbar-btn"><i class="fa fa-refresh"></i></button></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i> <i class="fa fa-angle-down"></i></a>

						<ul class="dropdown-menu">
							<?php outLangList(); ?>
						</ul>
					</li>
				</ul>

				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav side-nav">
						<li><a href="/stats/server"><i class="fa fa-fw fa-area-chart"></i> <?php outLangStr('stix0001'); ?></a></li>
						<li><a href="/stats/mystats"><i class="fa fa-fw fa-bar-chart-o"></i> <?php outLangStr('stmy0001'); ?></a></li>
						<li><a href="/stats/addons/assign-groups"><i class="fa fa-fw fa-address-card-o"></i> <?php outLangStr('stag0001'); ?></a></li>
						<li>
							<a href="#top_collapse" data-toggle="collapse" data-taget="#top_collapse"><i class="fa fa-fw fa-trophy"></i> <?php outLangStr('sttw0001'); ?><span class="pull-right"><i class="fa fa-angle-down fa-fw"></i></span></a>

							<ul id="top_collapse" class="collapse">
								<li><a href="/stats/top/week"><?php outLangStr('sttw0002'); ?></a></li>
								<li><a href="/stats/top/month"><?php outLangStr('sttm0001'); ?></a></li>
								<li><a href="/stats/top/all"><?php outLangStr('stta0001'); ?></a></li>
							</ul>
						</li>
						<li><a href="/stats/list"><i class="fa fa-fw fa-list-ul"></i> <?php outLangStr('stnv0029'); ?></a></li>
						<li><a href="/stats/info"><i class="fa fa-fw fa-info-circle"></i> <?php outLangStr('stnv0030'); ?></a></li>
					</ul>
				</div>
			</nav>

			<div id="container">
				<div id="page-wrapper">
