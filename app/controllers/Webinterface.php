<?php
class Webinterface extends TSN_Controller {

	public function view($page = 'login') {
		if(!file_exists(APP_PATH.'views/webinterface/'.$page.'.php')) show_404();

		$this->load->view('_templates/header_wi');
		$this->load->view('webinterface/'.$page);
		$this->load->view('_templates/footer');
	}

	public function admin($page) {
		$this->view('admin/'.$page);
	}

	public function addons($addon) {
		$this->view('addons/'.$addon);
	}

	public function teamspeak() {
		$this->view('teamspeak');
	}

	public function database() {
		$this->view('database');
	}

	public function core() {
		$this->view('core');
	}

	public function others() {
		$this->view('others');
	}

	public function notifys() {
		$this->view('notifys');
	}

	public function stats() {
		$this->view('stats');
	}

	public function control() {
		$this->view('control');
	}

	public function chpassword() {
		$this->view('chpassword');
	}

	public function resetpassword() {
		$this->view('resetpassword');
	}
}
