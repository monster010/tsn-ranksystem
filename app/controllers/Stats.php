<?php
class Stats extends TSN_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('stats');
		$this->load->helper('general');
	}

	public function addons($addon) {
		$this->view('addons/'.$addon);
	}

	public function server() {
		$stats = $this->stats->getServerStats();
		$usage = $this->stats->getServerUsage();

		$this->view('server', array('server' => $stats, 'sv_usage' => $usage));
	}

	public function mystats() {
		$this->view('mystats');
	}

	public function top($type = 'week') {
		$this->view('top/'.$type);
	}

	public function list() {
		$this->view('list');
	}

	public function info() {
		$this->view('info');
	}

	public function platforms() {
		$this->view('platforms');
	}

	public function nations() {
		$this->view('nations');
	}

	public function versions() {
		$this->view('versions');
	}

	private function view($page = 'server', $params = array()) {
		if(!file_exists(APP_PATH.'views/stats/'.$page.'.php')) show_404();

		$this->load->view('_templates/header');
		$this->load->view('stats/'.$page, $params);
		$this->load->view('_templates/footer');
	}
}
