<?php
class Stats_model extends TSN_Model {

	public function getServerStats() {
		return $this->db->row("SELECT * FROM stats_server");
	}

	public function getServerUsage() {
		return $this->db->query("SELECT timestamp, clients, channel FROM server_usage ORDER BY timestamp DESC LIMIT 96");
	}
}
